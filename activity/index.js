let http = require('http');

let addcourse = [
	{
		"subject": "Filipino",
		"code": "Fili01"
	},
	{
		"subject": "Biology",
		"code": "Bio01"
	}
];

let port = 4000;

const server = http.createServer(function(request, response)
	
	{	
		if(request.url == '/profile' && request.method == 'GET') { 
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to your Profile!')
		} 
		else if(request.url == '/courses' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Here are courses available')
		} 
		else if(request.url == '/addcourses' && request.method == 'POST') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Add course to our resources')
		} 
		else if(request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to Booking System')
		}
	})

server.listen(port);

console.log(`Server is running at localhost: ${port}`);